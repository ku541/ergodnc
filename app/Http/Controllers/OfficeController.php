<?php

namespace App\Http\Controllers;

use App\Models\Office;
use App\Models\Reservation;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Resources\OfficeResource;
use Illuminate\Database\Eloquent\Builder;
USE Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class OfficeController extends Controller
{
    public function index(): AnonymousResourceCollection
    {
        $offices = Office::query()
            ->where('hidden', false)
            ->where('approval_status', Office::APPROVAL_APPROVED)
            ->when(request('user_id'),
                fn (Builder $builder)
                    => $builder->whereUserId(request('user_id'))
            )
            ->when(request('visitor_id'),
                fn (Builder $builder)
                    => $builder->whereRelation('reservations', 'user_id', '=', request('visitor_id'))
            )
            ->when(
                request('lat') && request('lng'),
                fn (Builder $builder) => $builder->nearestTo(request('lat'), request('lng')),
                fn (Builder $builder) => $builder->orderBy('id')
            )
            ->with(['images', 'tags', 'user'])
            ->withCount(['reservations' =>
                fn (Builder $builder) =>
                    $builder->whereStatus(Reservation::STATUS_ACTIVE)
            ])
            ->paginate(20);

        return OfficeResource::collection($offices);
    }

    public function store(): JsonResource
    {
        if (! auth()->user()->tokenCan('office.store')) {
            abort(Response::HTTP_FORBIDDEN);
        }

        $attributes = validator(request()->all(), [
            'title' => ['required', 'string'],
            'description' => ['string'],
            'lat' => ['required', 'numeric'],
            'lng' => ['required', 'numeric'],
            'address_line1' => ['required', 'string'],
            'hidden' => ['bool'],
            'price_per_day' => ['required', 'integer', 'min:100'],
            'monthly_discount' => ['integer', 'min:0', 'max:90'],

            'tags' => ['array'],
            'tags.*' => ['integer', Rule::exists('tags', 'id')]
        ])->validate();

        $attributes['approval_status'] = Office::APPROVAL_PENDING;

        $office = auth()->user()->offices()->create(Arr::except($attributes, ['tags']));

        $office->tags()->sync($attributes['tags']);

        return OfficeResource::make($office);
    }

    public function show(Office $office): JsonResource
    {
        $office->loadCount(['reservations' =>
            fn (Builder $builder) =>
                $builder->whereStatus(Reservation::STATUS_ACTIVE)
        ])->load(['images', 'tags', 'user']);

        return OfficeResource::make($office);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Office  $office
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Office $office)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Office  $office
     * @return \Illuminate\Http\Response
     */
    public function destroy(Office $office)
    {
        //
    }
}
