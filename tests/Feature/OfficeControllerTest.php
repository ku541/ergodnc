<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Tag;
use App\Models\User;
use App\Models\Office;
use App\Models\Reservation;
use Illuminate\Foundation\Testing\RefreshDatabase;

class OfficeControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function it_lists_all_offices_in_a_paginated_way()
    {
        Office::factory(3)->create();

        $response = $this->get('/api/offices');

        $response->assertOk()
            ->assertJsonCount(3, 'data');

        $this->assertNotNull($response->json('data')[0]['id']);

        $this->assertNotNull($response->json('links'));

        $this->assertNotNull($response->json('meta'));
    }

    /**
     * @test
     */
    public function it_only_lists_offices_that_are_visible_and_approved()
    {
        Office::factory(3)->create();

        Office::factory()->create(['hidden' => true]);

        Office::factory()->create([
            'approval_status' => Office::APPROVAL_PENDING
        ]);

        $response = $this->get('/api/offices');

        $response->assertOk()
            ->assertJsonCount(3, 'data');
    }

    /**
     * @test
     */
    public function it_filters_by_user_id()
    {
        Office::factory(3)->create();

        $host = User::factory()->create();

        $office = Office::factory()->for($host)->create();

        $response = $this->get('/api/offices?user_id=' . $host->id);

        $response->assertOk()
            ->assertJsonCount(1, 'data');

        $this->assertEquals($office->id, $response->json('data')[0]['id']);
    }

    /**
     * @test
     */
    public function it_filters_by_visitor_id()
    {
        Office::factory(3)->create();

        $user = User::factory()->create();

        $office = Office::factory()->has(Reservation::factory())->create();

        Reservation::factory()->for(Office::factory())->create();

        Reservation::factory()->for($office)->for($user)->create();

        $response = $this->get('/api/offices?visitor_id=' . $user->id);

        $response->assertOk()
            ->assertJsonCount(1, 'data');

        $this->assertEquals($office->id, $response->json('data')[0]['id']);
    }

    /**
     * @test
     */
    public function it_includes_images_tags_and_user()
    {
        $user = User::factory()->create();

        $tag = Tag::factory()->create();

        $office = Office::factory()->for($user)->create();

        $office->tags()->attach($tag);

        $office->images()->create(['path' => 'image.jpg']);

        $response = $this->get('/api/offices');

        $response->assertOk();

        $this->assertIsArray($response->json('data')[0]['tags']);

        $this->assertCount(1, $response->json('data')[0]['tags']);

        $this->assertIsArray($response->json('data')[0]['images']);

        $this->assertCount(1, $response->json('data')[0]['images']);

        $this->assertEquals(
            $user->id, $response->json('data')[0]['user']['id']
        );
    }

    /**
     * @test
     */
    public function it_returns_the_number_of_active_reservations()
    {
        $office = Office::factory()->create();

        Reservation::factory()->for($office)->create([
            'status' => Reservation::STATUS_ACTIVE
        ]);

        Reservation::factory()->for($office)->create([
            'status' => Reservation::STATUS_CANCELLED
        ]);

        $response = $this->get('/api/offices');

        $response->assertOk();

        $this->assertEquals(1, $response->json('data')[0]['reservations_count']);
    }

    /**
     * @test
     */
    public function it_orders_by_distance_when_coordinates_are_provided()
    {
        Office::factory()->create([
            'lat' => '39.75641592073851',
            'lng' => '-8.806647697286472',
            'title' => 'Leiria'
        ]);

        Office::factory()->create([
            'lat' => '39.094757785845765',
            'lng' => '-9.259551518204294',
            'title' => 'Torres Vedras'
        ]);

        $response = $this->get('/api/offices?lat=38.72807300286458&lng=-9.139262950497757');

        $response->assertOk();

        $this->assertEquals('Torres Vedras', $response->json('data')[0]['title']);

        $this->assertEquals('Leiria', $response->json('data')[1]['title']);

        $response = $this->get('/api/offices');

        $response->assertOk();

        $this->assertEquals('Leiria', $response->json('data')[0]['title']);

        $this->assertEquals('Torres Vedras', $response->json('data')[1]['title']);
    }

    /**
     * @test
     */
    public function it_shows_the_office()
    {
        $user = User::factory()->create();

        $tag = Tag::factory()->create();

        $office = Office::factory()->for($user)->create();

        $office->tags()->attach($tag);

        $office->images()->create(['path' => 'image.jpg']);

        Reservation::factory()->for($office)->create([
            'status' => Reservation::STATUS_ACTIVE
        ]);

        Reservation::factory()->for($office)->create([
            'status' => Reservation::STATUS_CANCELLED
        ]);

        $response = $this->get('/api/offices/' . $office->id);

        $response->assertOk();

        $this->assertEquals(1, $response->json('data')['reservations_count']);

        $this->assertIsArray($response->json('data')['tags']);

        $this->assertCount(1, $response->json('data')['tags']);

        $this->assertIsArray($response->json('data')['images']);

        $this->assertCount(1, $response->json('data')['images']);

        $this->assertEquals(
            $user->id, $response->json('data')['user']['id']
        );
    }

    /**
     * @test
     */
    public function it_creates_an_office()
    {
        $user = User::factory()->create();
        $tag1 = Tag::factory()->create();
        $tag2 = Tag::factory()->create();

        $this->actingAs($user);

        $response = $this->postJson('/api/offices', [
            'title' => 'Title',
            'description' => 'Description',
            'lat' => '39.75641592073851',
            'lng' => '-8.806647697286472',
            'address_line1' => 'Address',
            'price_per_day' => 10_000,
            'monthly_discount' => 5,
            'tags' => [$tag1->id, $tag2->id]
        ]);

        $response->assertCreated()
            ->assertJsonPath('data.title', 'Title')
            ->assertJsonPath('data.approval_status', Office::APPROVAL_PENDING)
            ->assertJsonPath('data.user.id', $user->id)
            ->assertJsonCount(2, 'data.tags');

        $this->assertDatabaseHas('offices', ['title' => 'Title',]);
    }

    /**
     * @test
     */
    public function it_does_not_allow_creating_if_scope_is_not_provided()
    {
        $user = User::factory()->create();

        $token = $user->createToken('test', []);

        $response = $this->postJson('/api/offices', [], [
            'Authorization' => 'Bearer ' . $token->plainTextToken
        ]);

        $response->assertForbidden();
    }
}
